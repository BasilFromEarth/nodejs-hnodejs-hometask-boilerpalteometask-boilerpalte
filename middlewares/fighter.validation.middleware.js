const { fighter } = require('../models/fighter');
const FighterService = require('../services/fighterService');

const createFighterValid = (req, res, next) => {
    try {
        let {id, health, ...fighterValidation} = fighter;
        
        if(!(JSON.stringify(Object.keys(fighterValidation).sort()) === JSON.stringify(Object.keys(req.body).sort()))){
            throw Error('Not all fields valid or id in body');
        }

        let powerValidation = Number(req.body.power);

        if(powerValidation > 99 || powerValidation < 1 || isNaN(powerValidation)){
            throw Error('Power have to be number between 1 and 99');
        } 
        let defenseValidation = Number(req.body.defense);

        if(defenseValidation > 10 || defenseValidation < 1 || isNaN(defenseValidation)){
            throw Error('Defense have to be number between 1 and 10');
        } 
    } catch (err) {
        res.err = err;
        res.isError = true;
    } finally {
        next();
    }
}

const updateFighterValid = (req, res, next) => {
    try {
        if(!(FighterService.getOne({id: req.params.id}))){
            throw Error('Fighter doesn`t exist');
        }
        
        let {id, ...fighterValidation} = fighter;
        
        for(field in req.body){
            if(!(field in fighterValidation)){
                throw Error('There are some invalid fields');
            }
            if(field == "power"){  
                let powerValidation = Number(req.body.power);

                if(powerValidation > 99 || powerValidation < 1 || isNaN(powerValidation)){
                    throw Error('Power have to be number between 1 and 99');
                }
            }
            if(field == "defense"){  
                let defenseValidation = Number(req.body.defense);

                if(defenseValidation > 10 || defenseValidation < 1 || isNaN(defenseValidation)){
                    throw Error('Defense have to be number between 1 and 10');
                }
            }
        }
    } catch (err) {
        res.err = err;
        res.isError = true;
    } finally {
        next();
    }
    
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;