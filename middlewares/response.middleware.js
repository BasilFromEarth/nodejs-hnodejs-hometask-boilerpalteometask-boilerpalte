const responseMiddleware = (req, res, next) => {
    if(res.isError){
        res.status(400).json({ error: 'true', message: res.err.message })
    } else  if(res.data == 0 || res.data == null){
        res.status(404).json({ error: 'true', message: "Not found" })
    } else {        
        res.status(200).send(res.data);
    }
}

exports.responseMiddleware = responseMiddleware;