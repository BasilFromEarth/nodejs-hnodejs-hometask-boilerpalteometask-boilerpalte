const { user } = require('../models/user');
const UserService = require('../services/userService');
const createUserValid = (req, res, next) => {
    try {
        let {id, ...userValidation} = user;
    
        if(!(JSON.stringify(Object.keys(userValidation).sort()) === JSON.stringify(Object.keys(req.body).sort()))){
            throw Error('Not all fields valid or id in body');
        }

        let emailValidation = req.body.email.slice(-10);

        
        if(emailValidation !== "@gmail.com"){
            throw Error('Only gmail allowed');
        }

        let numberValidation1 = req.body.phoneNumber.slice(0, 4);
        let numberValidation2 = req.body.phoneNumber.slice(4);

        if(numberValidation1 !== "+380" || isNaN(numberValidation2) || numberValidation2.length != 9){
            throw Error('Number format is +380xxxxxxxxx');
        }
    
        if(UserService.getOne({email: req.body.email})) {
            throw Error("User with such mail already exist");
        } else if (UserService.getOne({phoneNumber: req.body.phoneNumber})) {
            throw Error("User with such phone number already exist");
        } 
    } catch (err) {
        res.err = err;
        res.isError = true;
    } finally {
        next();
    }
    
}

const updateUserValid = (req, res, next) => {
    try {
        if(!(UserService.getOne({id: req.params.id}))){
            throw Error('User doesn`t exist');
        }

        let {id, ...userValidation} = user;
        
        for(field in req.body) {
            if(!(field in userValidation)){
                throw Error('Not all fields valid or id in body');
            }
        }
        if('email' in req.body){
            let emailValidation = req.body.email.slice(-10);
   
            if(emailValidation !== "@gmail.com"){
                throw Error('Only gmail allowed');  
            }
        } 
        if('phoneNumber' in req.body){
            let numberValidation1 = req.body.phoneNumber.slice(0, 4);
            let numberValidation2 = req.body.phoneNumber.slice(4);

            if(numberValidation1 !== "+380" || isNaN(numberValidation2) || numberValidation2.length != 9){
                throw Error('Number format is +380xxxxxxxxx');
            }
        }
    } catch (err) {
        res.err = err;
        res.isError = true;
    } finally {
        next();
    }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;