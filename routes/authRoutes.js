const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
        user = AuthService.login(req.body);
        res.data = user;
    } catch (err) {
        res.err = err;
        res.isError = true;
    } finally {
        next()
    }
}, responseMiddleware);

module.exports = router;