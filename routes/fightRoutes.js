const { Router } = require('express');
const FightService = require('../services/fightService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');


const router = Router();

// OPTIONAL TODO: Implement route controller for fights

router.post('/start', (req, res, next) => {
    try {
        newFight = FightService.create(req.body)
        res.data = newFight;
    } catch (err) {
        res.err = err;
        res.isError = true;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;