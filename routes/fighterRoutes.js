const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();


router.post('/', createFighterValid, (req, res, next) => {
    try {
        if(!res.isError){
            newFighter = FighterService.create(req.body)
            res.data = newFighter;
        }
    } catch (err) {
        res.err = err;
        res.isError = true;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/', (req, res, next) => {
    try {
        allFighters = FighterService.getAll();
        res.data = allFighters;   
    } catch (err) {
        res.err = err;     
        res.isError = true;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        fighter = FighterService.getOne(req.params);
        res.data = fighter;   
    } catch (err) {
        res.err = err;
        res.isError = true;
    } finally {
        next();
    }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {
        deletedFighter = FighterService.delete(req.params.id);
        res.data = deletedFighter;   
    } catch (err) {
        res.err = err;
        res.isError = true;
    } finally {
        next(); 
    }
}, responseMiddleware);

router.put('/:id', updateFighterValid, (req, res, next) => {
    try {
        if(!res.isError){
            updatedFighter = FighterService.update(req.params.id, req.body)
            res.data = updatedFighter;
        }
    } catch (err) {
        res.err = err;
        res.isError = true;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;