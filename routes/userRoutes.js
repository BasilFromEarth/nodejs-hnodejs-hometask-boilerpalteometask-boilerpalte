const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/', createUserValid, (req, res, next) => {
    try {
        if(!res.isError){
            newUser = UserService.create(req.body)
            res.data = newUser;
        }
    } catch (err) {
        res.err = err;
        res.isError = true;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/', (req, res, next) => {
    try {
        allUsers = UserService.getAll();
        res.data = allUsers;   
    } catch (err) {
        res.err = err;
        res.isError = true;
    } finally {
        next();
    }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
    try {
        user = UserService.getOne(req.params);
        res.data = user;
    } catch (err) {
        res.err = err;
        res.isError = true;
    } finally {
        next();
    }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
    try {
        deletedUser = UserService.delete(req.params.id);
        res.data = deletedUser;   
    } catch (err) {
        res.err = err;
        res.isError = true;
    } finally {
        next(); 
    }
}, responseMiddleware);

router.put('/:id', updateUserValid, (req, res, next) => {
    try {
        if(!res.isError){
            updatedUser = UserService.update(req.params.id, req.body)
            res.data = updatedUser;
        }
    } catch (err) {
        res.err = err;
        res.isError = true;
    } finally {
        next();
    }
}, responseMiddleware);


module.exports = router;