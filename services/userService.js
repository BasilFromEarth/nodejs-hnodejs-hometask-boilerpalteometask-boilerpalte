const { UserRepository } = require('../repositories/userRepository');

class UserService {

    getAll(){
        const item = UserRepository.getAll();
        return item;
    }

    getOne(search){
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    create(data){
        const item = UserRepository.create(data);
        return item;
    }

    update(id, dataToUpdate){
        const item = UserRepository.update(id, dataToUpdate)
        if(!item) {
            return null;
        }
        return item;
    }

    delete(id){
        const item = UserRepository.delete(id);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();